package org.firstinspires.ftc.teamcode.Slider;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

public class SliderBlocker {

    public enum SLIDER_BLOCKER_STATE {
        BLOCK,
        UNBLOCK
    }

    private final Servo mServo;

    private final float blockPosition = 0f;
    private final float unblockPosotion = 0.5f;
    private final int   sleepTime = 300;


    public SLIDER_BLOCKER_STATE currentState;

    public SliderBlocker(LinearOpMode opMode){
        this.mServo = opMode.hardwareMap.get(Servo.class, "slider_blocker_servo");
    }

    public void init(){
        /*currentState = SLIDER_BLOCKER_STATE.BLOCK;*/
        try {
            setState(SLIDER_BLOCKER_STATE.BLOCK);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setState(SLIDER_BLOCKER_STATE target) throws Exception {
        if(target == SLIDER_BLOCKER_STATE.BLOCK)        mServo.setPosition(blockPosition);
        if(target == SLIDER_BLOCKER_STATE.UNBLOCK)      mServo.setPosition(unblockPosotion);

        currentState = target;
    }

    public void setStateAndWait(SLIDER_BLOCKER_STATE target) throws Exception {
        setState(target);
        Thread.sleep(sleepTime);
    }


    public String getStatus(){
        if(currentState == SLIDER_BLOCKER_STATE.BLOCK)   return "Slider Blocker : block ";
        if(currentState == SLIDER_BLOCKER_STATE.UNBLOCK) return "Slider Blocker : unblock ";
        return " REVIEW SliderBlocker CLASS!! ";
    }


}
