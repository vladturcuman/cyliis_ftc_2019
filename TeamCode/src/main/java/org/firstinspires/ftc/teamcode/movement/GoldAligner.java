package org.firstinspires.ftc.teamcode.movement;

import android.util.Pair;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Tamara;

public class GoldAligner implements ITurner {
    final double toPosition;

    static double SPEED = 0.2;
    static double PRECISION_ERROR = 3;

    Tamara tamara;
    private static LinearOpMode opMode;

    public GoldAligner(double position){
        tamara = Tamara.getInstance();
        toPosition = position;
        opMode = tamara.opMode;
    }
    @Override
    public Pair<Boolean, Double> getError() {
        Double gold = tamara.tfod.getGoldMineralPosition();

        while(opMode.opModeIsActive() && gold == null){
            //tamara.drivetrain.stopMotors();
            gold = tamara.tfod.getGoldMineralPosition();

            opMode.telemetry.addLine("why the fuck have you entered here");
            opMode.telemetry.update();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //tamara.drivetrain.resumeMotors();

        Double robotError = toPosition - gold;

        int dir;
        if(robotError < 0) dir = -1;
        else dir = 1;

        if (Math.abs(robotError) <= PRECISION_ERROR) {
            return new Pair<>(true, 0d);
        }


        return new Pair<>(false, SPEED * dir);
    }
}
