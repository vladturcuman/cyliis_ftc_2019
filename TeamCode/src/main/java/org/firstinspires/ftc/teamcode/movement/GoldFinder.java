package org.firstinspires.ftc.teamcode.movement;

import android.text.TextUtils;
import android.util.Pair;

import org.firstinspires.ftc.teamcode.Tamara;
import org.firstinspires.ftc.teamcode.utils.Formulas;

public class GoldFinder implements ITurner {
    static final double SPEED = 0.4;
    static final double PRECISION_ERROR = 5;

    private final double leftMargin;
    private final double rightMargin;
    int dir;

    Tamara tamara;

    public GoldFinder(double range){
        tamara = Tamara.getInstance();
        double myPosition = tamara.drivetrain.imu.getAngularOrientation().firstAngle;
        dir = 1;
        leftMargin = Formulas.ajustAngle(myPosition + range / 2);
        rightMargin = Formulas.ajustAngle(myPosition - range / 2);
    }

    @Override
    public Pair<Boolean, Double> getError() {
        double myPosition = tamara.drivetrain.imu.getAngularOrientation().firstAngle;

        if(dir == 1)
            if(Math.abs(myPosition - leftMargin) <= PRECISION_ERROR)
                dir = -1;

        if(dir == -1)
            if(Math.abs(myPosition - rightMargin) <= PRECISION_ERROR)
                dir = 1;

        if(Tamara.getInstance().tfod.getGoldMineralPosition() != null) return new Pair<>(true, 0d);
        else return new Pair<>(false, SPEED * dir);
    }
}
