package org.firstinspires.ftc.teamcode.Lift;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

public class LiftBlocker {

    public enum LIFT_BLOCKER_STATE{
        BLOCKED,
        UNBLOCKED,
        UNKNOWN
    }

    private final Servo mServo;

    private final float blockedPosition = 0.1f;
    private final float unblockedPosotion = 0f;
    private final int   sleepTime = 500;

    public LIFT_BLOCKER_STATE currentState;

    public LiftBlocker(LinearOpMode opMode){
        this.mServo = opMode.hardwareMap.get(Servo.class, "lift_blocker_servo");
        currentState = LIFT_BLOCKER_STATE.UNKNOWN;
    }

    public void init(){

    }

    public void setState(LIFT_BLOCKER_STATE target) throws Exception {

        if(target == LIFT_BLOCKER_STATE.BLOCKED)   mServo.setPosition(blockedPosition  );
        if(target == LIFT_BLOCKER_STATE.UNBLOCKED) mServo.setPosition(unblockedPosotion);
        if(target == LIFT_BLOCKER_STATE.UNKNOWN)   throw new Exception("Blocker : ilegal state!");

        currentState = target;
    }

    public void setStateAndWait(LIFT_BLOCKER_STATE target) throws Exception {
        setState(target);
        Thread.sleep(sleepTime);
    }

    public String getStatus(){
        if(currentState == LIFT_BLOCKER_STATE.BLOCKED)   return "Lift blocker : blocked ";
        if(currentState == LIFT_BLOCKER_STATE.UNBLOCKED) return "Lift blocker : unblocked ";
        if(currentState == LIFT_BLOCKER_STATE.UNKNOWN)   return "Lift blocker : unknown ";
        return " REVIEW LiftBlocker CLASS!! ";
    }

}
