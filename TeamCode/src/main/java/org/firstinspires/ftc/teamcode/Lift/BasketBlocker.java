package org.firstinspires.ftc.teamcode.Lift;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

public class BasketBlocker {

    public enum BASKET_BLOCKER_STATE{
        BLOCK,
        UNBLOCK
    }

    private final Servo mServo;

    private final float blockPosition = 0.05f;
    private final float unblockPosotion = 0.3f;
    private final int   sleepTime = 500;


    public BASKET_BLOCKER_STATE currentState;

    public BasketBlocker(LinearOpMode opMode){
        this.mServo = opMode.hardwareMap.get(Servo.class, "basket_blocker_servo");
    }

    public void init(){
        currentState = BASKET_BLOCKER_STATE.BLOCK;
    }

    public void setState(BASKET_BLOCKER_STATE target) throws Exception {
        if(target == BASKET_BLOCKER_STATE.BLOCK)        mServo.setPosition(blockPosition);
        if(target == BASKET_BLOCKER_STATE.UNBLOCK)      mServo.setPosition(unblockPosotion);

        currentState = target;
    }

    public void setStateAndWait(BASKET_BLOCKER_STATE target) throws Exception {
        setState(target);
        Thread.sleep(sleepTime);
    }


    public String getStatus(){
        if(currentState == BASKET_BLOCKER_STATE.BLOCK)   return "Basket : block ";
        if(currentState == BASKET_BLOCKER_STATE.UNBLOCK) return "Basket : unblock ";
        return " REVIEW BasketBlocker CLASS!! ";
    }


}
