package org.firstinspires.ftc.teamcode.utils;

import com.qualcomm.hardware.rev.RevTouchSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.TouchSensor;

public class DcRunToSensorRunnable implements Runnable {


    final LinearOpMode  opMode;
    final DigitalChannel   sensor;
    final DcMotor       dc;
    final double        power;
    final boolean        beforeStart;

    public DcRunToSensorRunnable(boolean beforeStart, LinearOpMode opMode, DigitalChannel sensor, DcMotor dc, double power) {
        this.opMode = opMode;
        this.sensor = sensor;
        this.dc = dc;
        this.power = power;
        this.beforeStart = beforeStart;

        opMode.telemetry.addData("sensor", sensor.getState());
        opMode.telemetry.update();

        sensor.setMode(DigitalChannel.Mode.INPUT);
    }


    @Override
    public void run() {

        if(opMode == null) return;

        if(!  sensor.getState()) {
            dc.setPower(0);
            dc.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            return;
        }


        dc.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        dc.setPower(power);

        try {
            while((opMode.opModeIsActive() || beforeStart) && sensor.getState() == true && !Thread.interrupted()) {
                opMode.telemetry.addData("sensor", sensor.getState());
                opMode.telemetry.update();

                    Thread.sleep(20);
            }

            if((opMode.opModeIsActive() && !beforeStart)  || Thread.interrupted()) return;
        } catch (InterruptedException e) { }

        dc.setPower(0);
        dc.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }
}
