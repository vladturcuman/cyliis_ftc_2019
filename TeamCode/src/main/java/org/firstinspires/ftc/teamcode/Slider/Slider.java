package org.firstinspires.ftc.teamcode.Slider;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

public class Slider {

    public enum SLIDER_STATE{
        EXTENDED,
        COMPACT
    }

    public SLIDER_STATE currentState;

    private final LinearOpMode opMode;
    public final Extender extender;
    public final Collector collector;
    //private final SliderBlocker sliderBlocker;
    public final SliderJoint sliderJoint;

    private Thread currentThread;

    public Slider(LinearOpMode opMode){
        this.opMode = opMode;

        extender = new Extender( opMode );
        collector = new Collector( opMode );
        //sliderBlocker = new SliderBlocker( opMode );
        sliderJoint = new SliderJoint( opMode );

    }

    public void manualInit(){
        try {
            sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.IDLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        extender.init();
        currentState = SLIDER_STATE.EXTENDED;
    }

    public void autonomusInit(){

        extender.init();
        currentState = SLIDER_STATE.COMPACT;
    }

    public void setState(SLIDER_STATE target) throws InterruptedException {
        if(target == currentState) return;
        if(currentThread != null && currentThread.isAlive()) currentThread.join();

        if(target == SLIDER_STATE.EXTENDED)
            currentThread = new Thread(extended);
        else
            currentThread = new Thread(compact);
        currentThread.start();
        currentState = target;
    }

    public void moveWithAndWait(int counts){
        extender.moveWith(counts);
        while(opMode.opModeIsActive() && extender.isMoving());
    }

    public void setStateAndWait(SLIDER_STATE target) throws InterruptedException {
        setState(target);
        if(currentThread != null && currentThread.isAlive())
            currentThread.join();
    }

    public String getStatus(){
        return "Slider: " + currentState.toString() + '\n' +
                extender.getStatus() + '\n' +
                collector.getStatus() + '\n' +
                sliderJoint.getStatus() ;
    }

    private Runnable extended = new Runnable() {
        @Override
        public void run() {
            try {
            //    sliderBlocker.setState(SliderBlocker.SLIDER_BLOCKER_STATE.BLOCK);
                sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.IDLE);
                //collector.setState(Collector.COLLECTOR_STATE.INTAKE);
                extender.setStateAndWait(Extender.EXTENDER_STATE.EXTENDED);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private  Runnable compact = new Runnable() {
        @Override
        public void run() {
            try {
                collector.setState(Collector.COLLECTOR_STATE.STILL);
                Thread.sleep(200);
                sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.IDLE);
                extender.setStateAndWait(Extender.EXTENDER_STATE.COMPACT);
                if (!opMode.opModeIsActive()) return;
           //     sliderBlocker.setStateAndWait(SliderBlocker.SLIDER_BLOCKER_STATE.UNBLOCK);
                if (!opMode.opModeIsActive()) return;

            }catch (Exception e){

            }
        }
    };

}
