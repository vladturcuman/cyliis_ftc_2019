package org.firstinspires.ftc.teamcode.Slider;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

public class SliderJoint {

    public enum SLIDER_JOINT_STATE{
        COLLECT,
        DEPOSIT,
        IDLE
    }

    private final Servo mServo;

    private final float collectPosition = 0.595f;
    private final float idlePosition = 0.35f;
    private final float depositPosition = 0.03f;
    private final int   sleepTime = 500;


    public SLIDER_JOINT_STATE currentState;

    public SliderJoint(LinearOpMode opMode){
        this.mServo = opMode.hardwareMap.get(Servo.class, "slider_joint_servo");
        currentState = SLIDER_JOINT_STATE.DEPOSIT;
    }

    public void setState(SLIDER_JOINT_STATE target) throws Exception {
        if(target == SLIDER_JOINT_STATE.COLLECT)    mServo.setPosition(collectPosition);
        if(target == SLIDER_JOINT_STATE.DEPOSIT)    mServo.setPosition(depositPosition);
        if(target == SLIDER_JOINT_STATE.IDLE)    mServo.setPosition(idlePosition);


        currentState = target;
    }

    public void setStateAndWait(SLIDER_JOINT_STATE target) throws Exception {
        setState(target);
        Thread.sleep(sleepTime);
    }


    public String getStatus(){
        if(currentState == SLIDER_JOINT_STATE.DEPOSIT) return "Slider Joint : Deposit ";
        if(currentState == SLIDER_JOINT_STATE.COLLECT) return "Slider Joint : Collect ";
        if(currentState == SLIDER_JOINT_STATE.IDLE) return "Slider Joint : Idle ";
        return " REVIEW SliderJoint CLASS!! ";
    }

}
