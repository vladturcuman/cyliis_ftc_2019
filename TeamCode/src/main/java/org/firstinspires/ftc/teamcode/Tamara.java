package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Lift.Lift;
import org.firstinspires.ftc.teamcode.Slider.Slider;
import org.firstinspires.ftc.teamcode.Slider.SliderJoint;
import org.firstinspires.ftc.teamcode.Slider.TeamMarkerReleaser;
import org.firstinspires.ftc.teamcode.movement.Drivetrain;
import org.firstinspires.ftc.teamcode.utils.TFO;

public class Tamara {

    public enum ROBOT_STATE{
        COLLECTING,
        PREPARE_TO_SCORE,
        PREPARE_TO_LATCH,
        DEPOSIT,
        LATCH
    }

    public ROBOT_STATE currentState;

    private static Tamara instance;

    public final LinearOpMode opMode;

    private Thread currentThread;

    public final Lift lift;
    public final Slider slider;
    public final TFO tfod;
    public final Drivetrain drivetrain;
    public final TeamMarkerReleaser teamMarkerReleaser;

    public Tamara(LinearOpMode opMode){
        instance = this;
        this.opMode = opMode;
        lift = new Lift(opMode);
        slider = new Slider(opMode);
        drivetrain = new Drivetrain(opMode);
        teamMarkerReleaser = new TeamMarkerReleaser(opMode);
        tfod = new TFO(opMode);
    }

    public static Tamara getInstance(){
        //if(instance == null) instance = new Tamara();
        return instance;
    }

    public void manualInit(){
            slider.manualInit();
            lift.manualInit();
            currentState = ROBOT_STATE.COLLECTING;

    }

    public void autonomusInitUp(){
        try {
            lift.autonomusInitUp();
            slider.autonomusInit();

            currentState = ROBOT_STATE.LATCH;
            teamMarkerReleaser.init();
            tfod.init();
        } catch(Exception e){
            opMode.telemetry.addLine(e.toString());
        }

    }

    public void setState(ROBOT_STATE target) throws InterruptedException {
        if(currentState == target) return;

        if(currentThread != null && currentThread.isAlive())
            currentThread.join();

        if(!opMode.opModeIsActive()) return;

        switch (target){
            case DEPOSIT: currentThread = new Thread(deposit); currentThread.start(); currentThread.join();
            case COLLECTING: currentThread = new Thread(collecting); break;
            case PREPARE_TO_SCORE: currentThread = new Thread(prepareToScore); break;
            case PREPARE_TO_LATCH: currentThread = new Thread(prepareToLatch); break;
            case LATCH: currentThread = new Thread(latch); break;
        }

        currentThread.start();
    }

    public void setStateAndWait(ROBOT_STATE target) throws InterruptedException {
       setState(target);
        currentThread.join();
    }

    public String getStatus(){
        return "Tamara : " + currentState.toString() + '\n' +
                lift.getStatus() + '\n' +
                slider.getStatus();
    }

    public void kill(){
        tfod.kill();
        drivetrain.stopMotors();
    }

    private Runnable collecting = new Runnable() {
        @Override
        public void run() {
            try {

                switch (currentState) {
                    case PREPARE_TO_SCORE:
                        lift.setStateAndWait(Lift.LIFT_STATES.SCORE);
                        if(!opMode.opModeIsActive()) return;
                        Thread.sleep(1000);
                        if(!opMode.opModeIsActive()) return;
                    case LATCH:
                        lift.setStateAndWait(Lift.LIFT_STATES.PREPARE_TO_LATCH);
                        lift.setState(Lift.LIFT_STATES.NORMAL);
                        break;
                    case DEPOSIT:
                        lift.setStateAndWait(Lift.LIFT_STATES.NORMAL);
                        slider.setStateAndWait(Slider.SLIDER_STATE.EXTENDED);
                        break;
                    case PREPARE_TO_LATCH:
                        lift.setStateAndWait(Lift.LIFT_STATES.NORMAL);
                        //slider.setStateAndWait(Slider.SLIDER_STATE.EXTENDED);
                        break;

                        default: return;
                }



                currentState = ROBOT_STATE.COLLECTING;

            }catch(Exception e){}

        }
    };

    private Runnable latch = new Runnable() {
        @Override
        public void run() {
            try {

                switch (currentState) {
                    case PREPARE_TO_LATCH:
                        lift.setStateAndWait(Lift.LIFT_STATES.LATCH);
                        break;
                    default: return;
                }

                currentState = ROBOT_STATE.LATCH;

            }catch(Exception e){}

        }
    };

    private Runnable prepareToLatch = new Runnable() {
        @Override
        public void run() {
            try {

                switch (currentState) {
                    case COLLECTING:
                        slider.setStateAndWait(Slider.SLIDER_STATE.COMPACT);
                    case PREPARE_TO_SCORE:
                        lift.setStateAndWait(Lift.LIFT_STATES.PREPARE_TO_LATCH);
                        break;
                    default: return;
                }

                currentState = ROBOT_STATE.PREPARE_TO_LATCH;

            }catch(Exception e){}

        }
    };

    private Runnable prepareToScore = new Runnable() {
        @Override
        public void run() {
            try {

                switch (currentState) {
                    case COLLECTING:
                        slider.setStateAndWait(Slider.SLIDER_STATE.EXTENDED);
                        slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.IDLE);
                    case PREPARE_TO_LATCH:
                        lift.setStateAndWait(Lift.LIFT_STATES.PREPARE_TO_SCORE);
                        break;
                    default: return;
                }

                currentState = ROBOT_STATE.PREPARE_TO_SCORE;

            }catch(Exception e){}

        }
    };

    private Runnable deposit = new Runnable() {
        @Override
        public void run() {
            try {

                switch (currentState) {
                    case COLLECTING:
                        slider.setStateAndWait(Slider.SLIDER_STATE.COMPACT);
                        slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.DEPOSIT);
                        Thread.sleep(1300);
                        break;
                    default: return;
                }

                currentState = ROBOT_STATE.DEPOSIT;

            }catch(Exception e){}

        }
    };

}