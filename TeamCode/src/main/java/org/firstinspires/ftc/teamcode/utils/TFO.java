package org.firstinspires.ftc.teamcode.utils;

import android.util.Pair;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

public class TFO {
    LinearOpMode opMode;

    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private static final String VUFORIA_KEY = "AfPfJhX/////AAABmQofITvlXEPpgKWnxHUWlTlbvDvhARJWuas96HLeiHoYPhCRIP3/rolh4MJDuv484dt9Wmi5MBJpdlgy4gJ4JhIEs3H6VygkkCmNxm9yS1+gerjR0Y029dsMeEF66fG7vmMtMTc9UkgKw7WAV6mdl6xZJh/HgqhP54zHqL5j6Vb4zRw7QlfxozPKU6s1H5hWtWBhwSCofsHPFg+DcLchOQ15gc1UiV9CrkbcRcX2pQXfrTq4+FSlc/kmI6h8XjYKg8uYomc/xuMitbSIMGCg0efwyZhHNCy+X2PJA322rgabcQCRuzbqMZOOJlgtCZeOJwdZq0r3ekydnTAtzDZry6C2uPXRbiI8aGmiomskazzc";
    private VuforiaLocalizer vuforia;

    private TFObjectDetector tfod;
    private static final double CONFIDENCE_LEVEL = 0.65;

    private boolean active;
    private int nrObj;

    private int lastKnownLX;
    private int lastKnownRX;
    private double lastKnownAngle;
    private double screenSize;

    public TFO(LinearOpMode opMode){
        this.opMode = opMode;

    }

    public void init() throws Exception{
        initVuforia();

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            opMode.telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        }

        opMode.telemetry.addData("TFO", "init");

        activate();
    }

    public void activate() throws Exception{
        opMode.telemetry.addData("TFO", "activate");
        opMode.telemetry.update();
        tfod.activate();
        active = true;
    }

    public void kill(){
        tfod.shutdown();
        active = false;
    }

    public int objectsDetected(){
        List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
        if (updatedRecognitions != null) {
            nrObj = updatedRecognitions.size();
            return updatedRecognitions.size();
        }

        return nrObj;
    }

    public double getScreenWidth(){
        return screenSize;
    }

    public Double getGoldMineralPosition(){
        if (tfod != null && active) {
            List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
            if (updatedRecognitions != null) {
                lastKnownLX = -1;
                lastKnownRX = -1;
                lastKnownAngle = -1;
                if (updatedRecognitions.size() >= 1)
                    for (Recognition recognition : updatedRecognitions)
                        if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                            lastKnownLX = (int) recognition.getLeft();
                            lastKnownRX = (int) recognition.getRight();
                        }
            }
        }

        if(lastKnownRX == -1) return null;
        return new Double((lastKnownLX + lastKnownRX) / 2.0);
    }


    private void initVuforia() throws Exception{
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraName = opMode.hardwareMap.get(WebcamName.class, "Webcam 1");

        vuforia = ClassFactory.getInstance().createVuforia(parameters);
    }

    private void initTfod() {
        int tfodMonitorViewId = opMode.hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", opMode.hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minimumConfidence = CONFIDENCE_LEVEL;
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }

}
