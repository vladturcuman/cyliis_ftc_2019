package org.firstinspires.ftc.teamcode.Lift;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

public class BasketJoint {

    public enum BASKET_JOINT_STATE{
        COLLECT,
        SCORE
    }

    private final Servo upperServo;
    private final Servo lowerServo;

    private final float collectPositionUpperServo = 0.95f;
    private final float scorePositionUpperServo = 0.2f;

    private final float collectPositionLowerServo = 1f;
    private final float scorePositionLowerServo = 0.7f;

    private final int   sleepTime = 500;


    public BASKET_JOINT_STATE currentState;

    public BasketJoint(LinearOpMode opMode){
        this.upperServo = opMode.hardwareMap.get(Servo.class, "basket_joint_upper_servo");
        this.lowerServo = opMode.hardwareMap.get(Servo.class, "basket_joint_lower_servo");
    }

    public void init(){
        currentState = BASKET_JOINT_STATE.COLLECT;
    }

    public void setState(BASKET_JOINT_STATE target) throws Exception {
        if(target == currentState) return;
        if(target == BASKET_JOINT_STATE.COLLECT) {
            upperServo.setPosition(collectPositionUpperServo);
            lowerServo.setPosition(collectPositionLowerServo);
        }
        if(target == BASKET_JOINT_STATE.SCORE) {
            upperServo.setPosition(scorePositionUpperServo);

            lowerServo.setPosition(0.85f);
            Thread.sleep(100);

            lowerServo.setPosition(scorePositionLowerServo);
        }

        currentState = target;
    }

    public void setStateAndWait(BASKET_JOINT_STATE target) throws Exception {
        setState(target);
        Thread.sleep(sleepTime);
    }


    public String getStatus(){
        if(currentState == BASKET_JOINT_STATE.SCORE)   return "Basket : Score ";
        if(currentState == BASKET_JOINT_STATE.COLLECT) return "Basket : Collect ";
        return " REVIEW BasketJoint CLASS!! ";
    }


}
