package org.firstinspires.ftc.teamcode.modes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Slider.Collector;
import org.firstinspires.ftc.teamcode.Slider.Extender;
import org.firstinspires.ftc.teamcode.Slider.Slider;
import org.firstinspires.ftc.teamcode.Slider.SliderJoint;
import org.firstinspires.ftc.teamcode.Tamara;
import org.firstinspires.ftc.teamcode.movement.AngleTurner;
import org.firstinspires.ftc.teamcode.movement.GoldAligner;
import org.firstinspires.ftc.teamcode.movement.GoldFinder;

@Autonomous(name = "Autonomus", group = "Linear Opmode")
public class AutonomusMode extends LinearOpMode {
    protected Tamara tamara;
    double screenSize = 800;



    @Override
    public void runOpMode() throws InterruptedException {
        tamara = new Tamara(this);

        tamara.autonomusInitUp();

        telemetry.addData("", tamara.getStatus());
        telemetry.update();

        waitForStart();


//        telemetry.addData("", "a");
//        telemetry.update();

        //tamara.lift.goDownAutonomus();

        //tamara.drivetrain.translate(-135, 135, 10);

//        if(true)
//            return;

        tamara.drivetrain.turnUntil(new GoldFinder(80));
        tamara.drivetrain.turnUntil(new GoldAligner(screenSize / 2));

        try {
            tamara.slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.COLLECT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tamara.slider.collector.setState(Collector.COLLECTOR_STATE.INTAKE);

        tamara.drivetrain.moveWith(tamara.drivetrain.imu.getAngularOrientation().firstAngle, 40);
        tamara.drivetrain.moveWith(tamara.drivetrain.imu.getAngularOrientation().firstAngle, -40);

        tamara.slider.collector.setState(Collector.COLLECTOR_STATE.STILL);

        try {
            tamara.slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.DEPOSIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
//
//
//        try {
//            tamara.slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.COLLECT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            tamara.slider.extender.setStateAndWait(Extender.EXTENDER_STATE.COMPACT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        tamara.slider.collector.setState(Collector.COLLECTOR_STATE.INTAKE);
//        tamara.slider.moveWithAndWait(1000000000);
//        sleep(1000);
//        tamara.slider.moveWithAndWait(-1000000000);

//        tamara.drivetrain.turnUntil(new AngleTurner(0));


        tamara.kill();
        //sleep(10000);

    }
}
