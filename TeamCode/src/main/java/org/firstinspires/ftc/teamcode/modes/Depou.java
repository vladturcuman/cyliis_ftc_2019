package org.firstinspires.ftc.teamcode.modes;


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.Slider.SliderJoint;
import org.firstinspires.ftc.teamcode.movement.AngleTurner;

@Autonomous(name = "Depou", group = "Linear Opmode")
public class Depou extends AutonomusMode {
    @Override
    public void runOpMode() throws InterruptedException {
        super.runOpMode();
        if(!opModeIsActive()) return;



        tamara.drivetrain.moveWith(-135, 30, 1);
        tamara.drivetrain.moveWith(-55, 105,1);
        //tamara.drivetrain.translate(-180, -90, -10);

        tamara.drivetrain.moveWith(-170, 20, 1);
        tamara.drivetrain.moveWith(-180, 85, 1);
        tamara.drivetrain.turnUntil(new AngleTurner(-90));
        tamara.teamMarkerReleaser.release();
        tamara.drivetrain.moveWith(0, 145, 1);

        try {
            tamara.slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.IDLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tamara.slider.extender.moveWith(1000);
    }
}
