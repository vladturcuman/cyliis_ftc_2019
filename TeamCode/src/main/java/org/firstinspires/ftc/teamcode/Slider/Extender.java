package org.firstinspires.ftc.teamcode.Slider;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;

import org.firstinspires.ftc.teamcode.Lift.Climber;
import org.firstinspires.ftc.teamcode.utils.DcPositionRunnable;
import org.firstinspires.ftc.teamcode.utils.DcRunToSensorRunnable;

public class Extender {


    public enum EXTENDER_STATE{
        COMPACT,
        EXTENDED
    }

    public EXTENDER_STATE currentState;
    public double        power          = -1;


    private final LinearOpMode opMode;
    private final DigitalChannel mSensor;
    private final DcMotor mDc;

    private boolean initialized;
    private int targetPosition;
    private final int minPosition = 100;
    private final int maxPosition = 6500;

    private final Runnable goCompact ;
    private Thread currentThread;

    private String message;

    public Extender(LinearOpMode opMode) {
        this.opMode = opMode;
        this.mDc = opMode.hardwareMap.get(DcMotor.class, "extender_dc");
        this.mSensor =  opMode.hardwareMap.get(DigitalChannel.class, "extender_sensor");
        initialized = false;

        goCompact = new DcPositionRunnable(opMode, 0, mDc, power);
    }

    public void init(){
        if(currentThread!= null && currentThread.isAlive()) {
            try {
                currentThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        currentState = EXTENDER_STATE.COMPACT;

        initialized = true;
        if(!mSensor.getState()) {
            mDc.setPower(0);
            mDc.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            return;
        }


        mDc.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        mDc.setPower(power);

        try {
            while(mSensor.getState() == true && !Thread.interrupted()) {
                opMode.telemetry.addData("init extender sensor:", mSensor.getState());
                opMode.telemetry.update();

                Thread.sleep(20);
            }
        } catch (InterruptedException e) { }

        mDc.setPower(0);
        mDc.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        initialized = true;
    }

    public String getStatus(){
        return "Extender : state -> " + currentState.toString() + "\n         encoder -> " +
                mDc.getCurrentPosition() + " from  " +  mDc.getTargetPosition() + (message!= null ? message : "");
    }

    public void setState(EXTENDER_STATE target) throws Exception {

        if(target == currentState) return;
        currentState = target;

        if(currentThread!= null && currentThread.isAlive()) {
            try {
                currentThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        switch(target){
            case COMPACT:
                currentThread = new Thread(goCompact);
                currentThread.start();
                break;
            case EXTENDED:
                if(initialized) {
                    targetPosition = minPosition;
                    mDc.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                    mDc.setPower(power);
                    mDc.setTargetPosition(minPosition);
                } else{
                    message = "nasol";
                }
                break;

        }
    }

    public void setStateAndWait(EXTENDER_STATE target) throws Exception {
        setState(target);

        if(currentThread != null && currentThread.isAlive())
            currentThread.join();

        while(opMode.opModeIsActive() && mDc.isBusy()) Thread.sleep(10);
    }

    public void moveWith(int counts) {
        try {
            if (currentState == EXTENDER_STATE.COMPACT)
                setStateAndWait(EXTENDER_STATE.EXTENDED);

            targetPosition = mDc.getCurrentPosition() + counts;
            if (targetPosition < minPosition) targetPosition = minPosition;
            if (targetPosition > maxPosition) targetPosition = maxPosition;

            mDc.setTargetPosition(targetPosition);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean isMoving() {
        return mDc.isBusy();
    }
}
