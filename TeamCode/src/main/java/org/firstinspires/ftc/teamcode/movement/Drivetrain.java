package org.firstinspires.ftc.teamcode.movement;

import android.util.Pair;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.utils.Formulas;

import Engine.Vector2;

public class Drivetrain {
    LinearOpMode opMode;


    private static final double    COUNTS_PER_MOTOR_NEVREST_40 = 1120;
    private static final double    COUNTS_PER_MOTOR_NEVREST_60 = 1680;
    private static final double    WHEEL_DIAMETER_CM    = 15.2;
    private static final double    DRIVE_GEAR_REDUCTION = 1.00;     // This is < 1.0 if geared UP
    private static final double    COUNTS_PER_CM        = (COUNTS_PER_MOTOR_NEVREST_60 * DRIVE_GEAR_REDUCTION) / (WHEEL_DIAMETER_CM * 3.1415);
    private static final double    HEADING_THRESHOLD       = 1.5;

    private static final double P_TRANSLATE_COEFF       = 0.03d;
    private static final double TRANSLATE_SPEED         = 0.6;

    private final DcMotor frontLeft;
    private final DcMotor frontRight;
    private final DcMotor backLeft;
    private final DcMotor backRight;

    private double frontRightLastPower;
    private double frontLeftLastPower;
    private double backRightLastPower;
    private double backLeftLastPower;
    private boolean stopped;

    private final double FAST = 1;
    private final double SLOW = 0.45;

    private final Gamepad gamepad;

    public BNO055IMU imu;

    public Drivetrain(LinearOpMode opMode){
        this.opMode = opMode;

        frontLeft   = opMode.hardwareMap.get(DcMotor.class, "front_left");
        frontRight  = opMode.hardwareMap.get(DcMotor.class, "front_right");
        backLeft    = opMode.hardwareMap.get(DcMotor.class, "back_left");
        backRight   = opMode.hardwareMap.get(DcMotor.class, "back_right");
        imu         = opMode.hardwareMap.get(BNO055IMU.class, "imu");

        setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeft.setDirection(DcMotorSimple.Direction.REVERSE);
        backLeft.setDirection(DcMotorSimple.Direction.REVERSE);

        initializeIMU();
        gamepad = opMode.gamepad1;
    }

    private void initializeIMU(){

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;

        imu.initialize(parameters);

        while(opMode.opModeIsActive() && !imu.isGyroCalibrated())
            opMode.sleep(50);
    }

    private void setPower(double speedFlBr, double speedFrBl){
        speedFlBr = Range.clip(speedFlBr, -1, 1);
        speedFrBl = Range.clip(speedFrBl,-1, 1);

        frontLeft.setPower(speedFlBr);
        frontRight.setPower(speedFrBl);
        backLeft.setPower(speedFrBl);
        backRight.setPower(speedFlBr);
    }

    private void setPowerTurn(double speedL, double speedR){
        speedL = Range.clip(speedL, -1, 1);
        speedR = Range.clip(speedR,-1, 1);

        frontLeft.setPower(speedL);
        frontRight.setPower(speedR);
        backLeft.setPower(speedL);
        backRight.setPower(speedR);
    }

    public void stopMotors(){
        frontRightLastPower = frontRight.getPower();
        frontLeftLastPower = frontLeft.getPower();
        backRightLastPower = backRight.getPower();
        backLeftLastPower = backLeft.getPower();

        frontRight.setPower(0);
        frontLeft.setPower(0);
        backRight.setPower(0);
        backLeft.setPower(0);

        stopped = true;
    }

    public void resumeMotors(){
        if(!stopped) return;

        frontRight.setPower(frontRightLastPower);
        frontLeft.setPower(frontLeftLastPower);
        backRight.setPower(backRightLastPower);
        backLeft.setPower(backLeftLastPower);

        frontRightLastPower = 0;
        frontLeftLastPower = 0;
        backRightLastPower = 0;
        backLeftLastPower = 0;
    }

    private void setMode(DcMotor.RunMode runMode){
        frontLeft.setMode(runMode);
        frontRight.setMode(runMode);
        backLeft.setMode(runMode);
        backRight.setMode(runMode);
    }

    private boolean areMotorsBusy(){
        return  Math.abs(frontLeft.getCurrentPosition()  - frontLeft.getTargetPosition()) > 5 &&
                Math.abs(frontRight.getCurrentPosition() - frontRight.getTargetPosition()) > 5 &&
                Math.abs(backLeft.getCurrentPosition()   - backLeft.getTargetPosition()) > 5 &&
                Math.abs(backRight.getCurrentPosition()  - backRight.getTargetPosition()) > 5;
    }

    public synchronized void turnWith(double angle) throws InterruptedException{
        double robotAngle = imu.getAngularOrientation().firstAngle;
        Double toAngle = robotAngle + angle;
        Formulas.ajustAngle(toAngle);
        turnUntil(new AngleTurner(toAngle));
    }

    public synchronized void turnUntil(ITurner checker) throws InterruptedException{
        while (opMode.opModeIsActive() && !onHeading(checker) && !Thread.interrupted()) {
            try{ Thread.sleep(1); }catch (InterruptedException e){
                stopMotors();
                throw e;
            }
        }
    }

    private boolean onHeading(ITurner checker) {
        boolean  onTarget = false ;
        double speed;

        Pair<Boolean, Double> ok = checker.getError();

        if (ok.first == true) onTarget = true;

        speed  = ok.second;
        setPowerTurn(-speed, speed);

        return onTarget;
    }

    public void startManual(){
        Thread manualDrivetrain = new Thread(manualMovement);
        manualDrivetrain.start();
    }

    private Runnable manualMovement = new Runnable() {
        @Override
        public void run() {
            while(opMode.opModeIsActive()){
                double speed;

                if (gamepad.a) speed = FAST;
                else speed = SLOW;

                double rotation = (gamepad.right_trigger - gamepad.left_trigger);
                //double rotation = 0;
                Vector2 p = DirectionToDCMotorsPowerMecanum(new Vector2(gamepad.left_stick_x, -gamepad.left_stick_y));

                double fl = p.getX() + rotation;
                double fr = p.getY() - rotation;
                double bl = p.getY() + rotation;
                double br = p.getX() - rotation;


                frontLeft.setPower(Range.clip(fl, -1, 1) * speed);
                frontRight.setPower(Range.clip(fr, -1, 1) * speed);
                backLeft.setPower(Range.clip(bl, -1, 1) * speed);
                backRight.setPower(Range.clip(br, -1, 1) * speed);

                try{
                    Thread.sleep(10);
                }catch (Exception e){}
            }
        }
    };

    private Vector2 DirectionToDCMotorsPowerMecanum(Vector2 direction) throws ArithmeticException{

        float angle = (float)Math.atan2(direction.getY(), direction.getX()) * 57.2957795f; // Angle in Deg
        angle -= 45; // Rotate with 45 deg
        angle *= 0.0174532925f;

        return new Vector2( (float)Math.cos((double)angle) * direction.length(),
                (float)Math.sin((double)angle) * direction.length());
    }

    void initializeMovementWith(double distance){

        int moveCounts = (int)(distance * COUNTS_PER_CM);

        int newFrontLeftTarget  = frontLeft.getCurrentPosition() + moveCounts;
        int newFrontRightTarget = frontRight.getCurrentPosition() + moveCounts;
        int newBackLeftTarget   = backLeft.getCurrentPosition() + moveCounts;
        int newBackRightTarget  = backRight.getCurrentPosition() + moveCounts;

        frontLeft.setTargetPosition(newFrontLeftTarget);
        frontRight.setTargetPosition(newFrontRightTarget);
        backLeft.setTargetPosition(newBackLeftTarget);
        backRight.setTargetPosition(newBackRightTarget);

        setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public synchronized void moveWith(/*double faceAngle, */double direction, double distance) throws InterruptedException {
        moveWith(/*faceAngle,*/ direction, distance, TRANSLATE_SPEED);
    }

    public synchronized void moveWith(/*double faceAngle, */double direction, double distance, double speed) throws InterruptedException {

        AngleTurner angleCheck = new AngleTurner(direction);

        turnUntil(angleCheck);

        initializeMovementWith(distance);


        double flSpeed = speed;
        double frSpeed = speed;
        double blSpeed = speed;
        double brSpeed = speed;

        frontLeft.setPower(flSpeed);
        frontRight.setPower(frSpeed);
        backLeft.setPower(blSpeed);
        backRight.setPower(brSpeed);

        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            stopMotors();
            setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            throw e;
        }

        while(opMode.opModeIsActive() && areMotorsBusy());
    //}

        stopMotors();
        setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    void initializeMovement(double distance){

        int moveCounts = (int)(distance * COUNTS_PER_CM);

        int newFrontLeftTarget  = frontLeft.getCurrentPosition() + moveCounts;
        int newFrontRightTarget = frontRight.getCurrentPosition() + moveCounts;
        int newBackLeftTarget   = backLeft.getCurrentPosition() + moveCounts;
        int newBackRightTarget  = backRight.getCurrentPosition() + moveCounts;

        frontLeft.setTargetPosition(newFrontLeftTarget);
        frontRight.setTargetPosition(newFrontRightTarget);
        backLeft.setTargetPosition(newBackLeftTarget);
        backRight.setTargetPosition(newBackRightTarget);

        setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public synchronized void translate(double faceAngle, double direction, double distance) throws InterruptedException {
        translate(faceAngle, direction, distance, TRANSLATE_SPEED);
    }

    public synchronized void translate(double faceAngle, double direction, double distance, double speed) throws InterruptedException{
        AngleTurner angleTurner = new AngleTurner(faceAngle);

        turnUntil(angleTurner);

        double fdSpeed = Math.cos(Math.toRadians(direction + 45));
        double sdSpeed = Math.sin(Math.toRadians(direction + 45));

        initializeMovement(distance);

        while (opMode.opModeIsActive() && areMotorsBusy() && !Thread.currentThread().interrupted()){
            double steer = angleTurner.getError().second;

            if (distance < 0)
                steer *= -1.0;

            double flSpeed = fdSpeed - steer;
            double frSpeed = sdSpeed + steer;
            double blSpeed = sdSpeed - steer;
            double brSpeed = fdSpeed + steer;

            // Normalize speeds if either one exceeds +/- 1.0;
            double max = Formulas.max(Math.abs(flSpeed), Math.abs(frSpeed),
                    Math.abs(blSpeed), Math.abs(brSpeed));

            if(max >= 1) {
                flSpeed /= max;
                frSpeed /= max;
                blSpeed /= max;
                brSpeed /= max;
            }

            if(flSpeed * (frontLeft.getTargetPosition() - frontLeft.getCurrentPosition()) < 0)
                frontLeft.setTargetPosition(frontLeft.getCurrentPosition() * 2 -
                        frontLeft.getTargetPosition());

            if(frSpeed * (frontRight.getTargetPosition() - frontRight.getCurrentPosition()) < 0)
                frontRight.setTargetPosition(frontRight.getCurrentPosition() * 2 -
                        frontRight.getTargetPosition());

            if(blSpeed * (backLeft.getTargetPosition() - backLeft.getCurrentPosition()) < 0)
                backLeft.setTargetPosition(backLeft.getCurrentPosition() * 2 -
                        backLeft.getTargetPosition());

            if(brSpeed * (backRight.getTargetPosition() - backRight.getCurrentPosition()) < 0)
                backRight.setTargetPosition(backRight.getCurrentPosition() * 2 -
                        backRight.getTargetPosition());

            frontLeft.setPower(flSpeed * speed);
            frontRight.setPower(frSpeed * speed);
            backLeft.setPower(blSpeed * speed);
            backRight.setPower(brSpeed * speed);

            try{ Thread.sleep(1); }
            catch (InterruptedException e){
                stopMotors();
                setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                throw e;
            }
        }

        stopMotors();
        setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }



}
