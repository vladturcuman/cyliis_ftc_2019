package org.firstinspires.ftc.teamcode.Lift;

import com.qualcomm.hardware.rev.RevTouchSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.TouchSensor;

import org.firstinspires.ftc.teamcode.utils.DcPositionRunnable;
import org.firstinspires.ftc.teamcode.utils.DcRunToSensorRunnable;

public class Climber {

    public enum CLIMBER_STATE{
        UNKNOWN,
        DOWN,
        DOWN_WITH_SENSOR,
        LATCH,
        SCORE,
        FALL
    }

    public CLIMBER_STATE currentState;
    public double        power          = 1;
    public int           latchPosition  = -4800;
    public int           scorePosition  = -6300;

    private final Runnable goDown;
    private final Runnable goToLatch;
    private final Runnable goToScore;
    private final Runnable goDownWithSensor;

    private final LinearOpMode  opMode;
    private final DigitalChannel mSensor;
    private final DcMotor       mDc;

    private Thread currentThread;
    private boolean initialized;
    private String message;


    public Climber(LinearOpMode opMode) {
        this.opMode = opMode;
        this.mDc = opMode.hardwareMap.get(DcMotor.class, "climber_dc");
        this.mSensor =  opMode.hardwareMap.get(DigitalChannel.class, "climber_sensor");
        initialized = false;
        currentState = CLIMBER_STATE.UNKNOWN;

        goDown           = new DcPositionRunnable  (opMode, 0,      mDc, power);
        goToLatch        = new DcPositionRunnable  (opMode, latchPosition, mDc, power);
        goToScore        = new DcPositionRunnable  (opMode, scorePosition, mDc, power);
        goDownWithSensor = new Thread(new DcRunToSensorRunnable (false, opMode, mSensor, mDc, power));
    }

    public void init(){

        mDc.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        Thread T = new Thread(new DcRunToSensorRunnable (true, opMode, mSensor, mDc, power));
        T.start();

        try { T.join(); } catch (InterruptedException e) { e.printStackTrace(); return; }

        currentState = CLIMBER_STATE.DOWN;
        initialized = true;

        opMode.telemetry.addData("init climber", "a");
        opMode.telemetry.update();

        mDc.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        mDc.setPower(power);
        mDc.setTargetPosition(0);
        mDc.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    public String getStatus(){
        return "Climber : state -> " + currentState.toString() + "\n         encoder -> " +
                mDc.getCurrentPosition() + " from  " +  mDc.getTargetPosition() + (message != null ? "\n" + message : " " );
    }

    public void setState(CLIMBER_STATE target) throws Exception {

        if(target == currentState) return;

        currentState = target;

        switch(target){
            case DOWN_WITH_SENSOR:
                setAction(goDownWithSensor);
                currentThread.join();
            case DOWN  :
                if(!initialized)
                    init();
                else
                    setAction(goDown);
                break;
            case LATCH : setAction(goToLatch);  break;
            case SCORE : setAction(goToScore);  break;
            case FALL  :
                initialized = false;
                mDc.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
                mDc.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                mDc.setPower(0);
                currentState = CLIMBER_STATE.DOWN;
                break;
        }

        currentState = target;
    }

    public void setStateAndWait(CLIMBER_STATE target) throws Exception {
        setState(target);

        if(currentThread != null)
            currentThread.join();
    }

    public boolean isMoving(){
        if(currentState == CLIMBER_STATE.UNKNOWN || !initialized) return false;
        return currentThread.isAlive();
    }

    private void setAction(Runnable action) throws Exception {
        if(!initialized) throw new Exception("Climber was not initialized");
        if(currentThread != null && currentThread.isAlive())
            currentThread.interrupt();
        if(currentThread != null) currentThread.join();
        currentThread = new Thread(action);
        currentThread.start();

    }

    public void kill(){
        currentThread.interrupt();
    }

}

