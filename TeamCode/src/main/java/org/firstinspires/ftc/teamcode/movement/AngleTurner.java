package org.firstinspires.ftc.teamcode.movement;

import android.util.Pair;

import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Tamara;
import org.firstinspires.ftc.teamcode.utils.Formulas;

public class AngleTurner implements ITurner {
    private final double toAngle;

    private static final double PRECISION_ERROR = 1.5;
    private static final double P_TURN_COEFF = 0.1;
    private static final double SPEED = 0.5;

    private Tamara tamara;

    public AngleTurner(double angle){
        tamara = Tamara.getInstance();
        toAngle = angle;
    }
    @Override
    public Pair<Boolean, Double> getError() {
        Double robotError;

        robotError = Formulas.ajustAngle(toAngle - tamara.drivetrain.imu.getAngularOrientation().firstAngle);


        if (Math.abs(robotError) <= PRECISION_ERROR) {
            return new Pair<>(true, 0d);
        }

        return new Pair<>(false, calculateSteer(robotError, P_TURN_COEFF) * SPEED);
    }



    private double calculateSteer(double error, double PCoeff) {
        return Range.clip(error * PCoeff, -1, 1);
    }
}
