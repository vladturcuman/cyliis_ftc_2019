package org.firstinspires.ftc.teamcode.Lift;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

public class Hook {

    public enum HOOK_STATE{
        BLOCKED,
        UNBLOCKED,
        UNKNOWN
    }

    private final Servo mServo;

    private final float blockedPosition = 0f;
    private final float unblockedPosotion = 0.5f;
    private final int   sleepTime = 500;


    public HOOK_STATE currentState;

    public Hook(LinearOpMode opMode){
        this.mServo = opMode.hardwareMap.get(Servo.class, "hook_servo");
        currentState = HOOK_STATE.UNKNOWN;
    }

    public void init(){

    }

    public void setState(HOOK_STATE target) throws Exception {
        if(target == HOOK_STATE.BLOCKED)   mServo.setPosition(blockedPosition);
        if(target == HOOK_STATE.UNBLOCKED) mServo.setPosition(unblockedPosotion);
        if(target == HOOK_STATE.UNKNOWN)   throw new Exception("Hook : ilegal state!");

        currentState = target;
    }

    public void setStateAndWait(HOOK_STATE target) throws Exception {
        setState(target);
        Thread.sleep(sleepTime);
    }


    public String getStatus(){
        if(currentState == HOOK_STATE.BLOCKED)   return "Hook : blocked ";
        if(currentState == HOOK_STATE.UNBLOCKED) return "Hook : unblocked ";
        if(currentState == HOOK_STATE.UNKNOWN)   return "Hook : unknown ";
        return " REVIEW HOOK CLASS!! ";
    }


}
