package org.firstinspires.ftc.teamcode.modes;

import com.qualcomm.hardware.rev.RevTouchSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

@TeleOp(name="Expansion hub debug", group="Linear Opmode")
public class testDrivetrain extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        DcMotor frontLeft   = hardwareMap.get(DcMotor.class, "front_left");
        DcMotor frontRight  = hardwareMap.get(DcMotor.class, "front_right");
        DcMotor backLeft    = hardwareMap.get(DcMotor.class, "back_left");
        DcMotor backRight   = hardwareMap.get(DcMotor.class, "back_right");

        frontLeft.setDirection(DcMotorSimple.Direction.REVERSE);
        backLeft.setDirection(DcMotorSimple.Direction.REVERSE);

        waitForStart();
        frontLeft.setPower(0.5);
        frontRight.setPower(0.5);
        backLeft.setPower(0.5);
        backRight.setPower(0.5);

//        waitForStart();
//
//        DcMotor a = hardwareMap.get(DcMotor.class, "a");
//        DcMotor b = hardwareMap.get(DcMotor.class, "b");
//
//        a.setPower(0.5);
//        b.setPower(0.5);

        while(opModeIsActive());
    }
}
