package org.firstinspires.ftc.teamcode.utils;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public class DcPositionRunnable implements Runnable {

    final LinearOpMode  opMode;
    final DcMotor       dc;
    final double        maxPower;
    final int           target;


    public DcPositionRunnable(LinearOpMode opMode, int target, DcMotor dc, double maxPower) {
        this.opMode = opMode;
        this.target = target;
        this.dc = dc;
        this.maxPower = maxPower;
    }


    @Override
    public void run() {
        if(opMode == null) return;

        if(dc.getMode() != DcMotor.RunMode.RUN_TO_POSITION) dc.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        dc.setTargetPosition( target );
        try {
        while(opMode.opModeIsActive() && Math.abs(dc.getCurrentPosition() - dc.getTargetPosition()) > 150 && !Thread.interrupted())
          Thread.sleep(10);
        }
        catch (InterruptedException e)
        { e.printStackTrace(); }

    }
}
