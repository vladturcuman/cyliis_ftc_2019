package org.firstinspires.ftc.teamcode.Slider;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public class Collector {

    public enum COLLECTOR_STATE{
        INTAKE,
        OUTLET,
        STILL
    }

    private final LinearOpMode  opMode;
    private final DcMotor       mDC;
    private final double        power = 0.8;

    public COLLECTOR_STATE currentState;

    public Collector(LinearOpMode opMode) {
        this.opMode = opMode;
        mDC = opMode.hardwareMap.get(DcMotor.class, "collector_dc");
        mDC.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        currentState = COLLECTOR_STATE.STILL;
    }

    public void setState(COLLECTOR_STATE target){

        if(target == currentState) return;

        switch (target){
            case INTAKE: mDC.setPower( power  ); break;
            case OUTLET: mDC.setPower( -power ); break;
            case STILL : mDC.setPower( 0      ); break;
        }

        currentState = target;

    }

    public String getStatus(){
        return "Collector : " + currentState.toString();
    }

}
