package org.firstinspires.ftc.teamcode.utils;

public class Formulas {
    public static double ajustAngle(double angle){
        while (angle > 180)  angle -= 360;
        while (angle <= -180) angle += 360;

        return angle;
    }

    public static int compare(double d1, double d2){
        if(Math.abs(d1 - d2) <= 0.0001)    return 0;
        if(d1 > d2)                        return 1;
        else                               return -1;
    }

    public static <T extends Comparable> T max(T... items){
        T maxim = items[0];
        for(T item : items){
            if(maxim.compareTo(item) < 0)
                maxim = item;
        }
        return maxim;
    }
}
