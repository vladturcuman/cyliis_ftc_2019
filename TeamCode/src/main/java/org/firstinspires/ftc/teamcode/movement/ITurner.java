package org.firstinspires.ftc.teamcode.movement;

import android.util.Pair;

public interface ITurner {
    Pair<Boolean, Double> getError();
}
