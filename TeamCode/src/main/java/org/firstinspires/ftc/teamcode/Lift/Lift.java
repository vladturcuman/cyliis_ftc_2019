package org.firstinspires.ftc.teamcode.Lift;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

public class Lift {

    public enum LIFT_STATES{
        LATCH,
        PREPARE_TO_LATCH,
        NORMAL,
        PREPARE_TO_SCORE,
        SCORE
    }


    public LIFT_STATES currentState;
    public LIFT_STATES  targetState;

    private final BasketBlocker basketBlocker;
    private final LinearOpMode  opMode;
    private final BasketJoint   basketJoint;
    private final LiftBlocker   liftBlocker;
    private final Climber       climber;
    private final Hook          hook;

    private Thread currentThread;
    private String lastMessege;

    public Lift(LinearOpMode opMode){
        this.opMode = opMode;

        climber = new Climber(opMode);

        basketBlocker   = new BasketBlocker (opMode);
        basketJoint     = new BasketJoint   (opMode);
        liftBlocker     = new LiftBlocker   (opMode);
        hook            = new Hook          (opMode);


    }

    public void manualInit(){
        try {

            climber.init();
            liftBlocker.setState(LiftBlocker.LIFT_BLOCKER_STATE.UNBLOCKED);
            hook.setState(Hook.HOOK_STATE.UNBLOCKED);
            basketBlocker.setState(BasketBlocker.BASKET_BLOCKER_STATE.UNBLOCK);
            basketJoint.setState(BasketJoint.BASKET_JOINT_STATE.COLLECT);

            currentState = targetState = LIFT_STATES.NORMAL;

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void autonomusInitUp(){
        try {
            climber.init();
            basketBlocker.setState(BasketBlocker.BASKET_BLOCKER_STATE.UNBLOCK);
            liftBlocker.setState(LiftBlocker.LIFT_BLOCKER_STATE.BLOCKED);
            currentState = targetState = LIFT_STATES.LATCH;

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void goDownAutonomus(){
        climber.init();
        try {
            opMode.telemetry.addData("a", "b");
            liftBlocker.setStateAndWait(LiftBlocker.LIFT_BLOCKER_STATE.UNBLOCKED);

            opMode.telemetry.addData("a", "c");
            climber.setStateAndWait(Climber.CLIMBER_STATE.LATCH);
        } catch (Exception e) {
            e.printStackTrace();

            opMode.telemetry.addData("a", e.toString());
        }

    }

    public void goBackDownAutonomous(){
        try {
            climber.setState(Climber.CLIMBER_STATE.DOWN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setState(LIFT_STATES target) throws InterruptedException {
        if(target == targetState) return;

        if(currentThread != null && currentThread.isAlive())
            currentThread.join();

        targetState = target;

        switch(target){
            case PREPARE_TO_LATCH   : performAction( prepareToLatch );    break;
            case PREPARE_TO_SCORE   : performAction( prepareToScore );    break;
            case NORMAL             : performAction( normal         );    break;
            case LATCH              : performAction( latch          );    break;
            case SCORE              : performAction( score          );    break;
        }
    }

    public void setStateAndWait(LIFT_STATES target) throws InterruptedException {
        setState(target);
        if(currentThread != null && currentThread.isAlive())
            currentThread.join();
    }



    public void performAction(Runnable action) throws InterruptedException {

        currentThread = new Thread(action);
        currentThread.start();
        Thread.sleep(10);
    }


    public String getStatus(){
        return "Lift: state --> "  + currentState.toString() + "\n" +
                "            target state --> " + targetState.toString() + "\n" +
                ( lastMessege != null ? lastMessege + "\n" : "" ) +
                climber.getStatus() + "\n" +
                basketBlocker.getStatus() + "\n" +
                basketJoint.getStatus() + "\n" +
                hook.getStatus() + "\n" +
                liftBlocker.getStatus() + "\n" ;
    }

    private boolean shouldTerminate(LIFT_STATES state){
        return !opMode.opModeIsActive() || Thread.interrupted() || targetState != state;
    }

    public  Runnable normal = new Runnable() {
        @Override
        public void run() {

            try{

                switch (currentState){

                    case PREPARE_TO_SCORE:
                        basketBlocker.setState(BasketBlocker.BASKET_BLOCKER_STATE.UNBLOCK);

                    case SCORE:
                        basketJoint.setState(BasketJoint.BASKET_JOINT_STATE.COLLECT);

                    case PREPARE_TO_LATCH:
                        climber.setStateAndWait(Climber.CLIMBER_STATE.DOWN);

                        break;

                    case LATCH:
                        climber.setStateAndWait(Climber.CLIMBER_STATE.DOWN);
                        break;

                }

                if(shouldTerminate(LIFT_STATES.NORMAL)) return;
                currentState = targetState;

            } catch (Exception e){ }

        }
    };

    public  Runnable prepareToLatch = new Runnable() {
        @Override
        public void run() {
            try{

                switch (currentState){

                    case PREPARE_TO_SCORE:
                        basketBlocker.setState(BasketBlocker.BASKET_BLOCKER_STATE.UNBLOCK);
                        Thread.sleep(1000);
                        basketJoint.setState(BasketJoint.BASKET_JOINT_STATE.COLLECT);

                    case LATCH:
                    case NORMAL:
                        climber.setStateAndWait(Climber.CLIMBER_STATE.LATCH);
                        break;

                    default:
                        lastMessege = "Can not go to PREPARE_TO_LATCH from " + currentState.toString();
                        return;
                }


                if(shouldTerminate(LIFT_STATES.PREPARE_TO_LATCH)) return;
                currentState = targetState;

            } catch (Exception e){
                lastMessege = e.toString();
            }
        }
    };

    public  Runnable latch = new Runnable() {
        @Override
        public void run() {
            try{
                switch (currentState){

                    case PREPARE_TO_LATCH:

                        climber.setStateAndWait(Climber.CLIMBER_STATE.DOWN);

                        break;

                    default:
                        lastMessege = "Can not go to LATCH from " + currentState.toString();
                        return;
                }

                if(shouldTerminate(LIFT_STATES.LATCH)) return;
                currentState = targetState;

            } catch (Exception e){
                lastMessege = e.toString();
            }
        }
    };

    public  Runnable prepareToScore = new Runnable() {
        @Override
        public void run() {
            try{
                switch (currentState){

                    case PREPARE_TO_LATCH:
                    case NORMAL:

                        basketBlocker.setState(BasketBlocker.BASKET_BLOCKER_STATE.BLOCK);
                        climber.setStateAndWait(Climber.CLIMBER_STATE.SCORE);
                        if(shouldTerminate(LIFT_STATES.PREPARE_TO_SCORE)) return;
                        basketJoint.setState(BasketJoint.BASKET_JOINT_STATE.SCORE);

                        break;

                    default:
                        lastMessege = "Can not go to PREPARE_TO_SCORE from " + currentState.toString();
                        return;
                }

                if(shouldTerminate(LIFT_STATES.PREPARE_TO_SCORE)) return;
                currentState = targetState;

            } catch (Exception e){

                lastMessege = e.toString();
            }
        }
    };

    public  Runnable score = new Runnable() {
        @Override
        public void run() {
            try{
                switch (currentState){

                    case PREPARE_TO_SCORE:
                        basketBlocker.setStateAndWait(BasketBlocker.BASKET_BLOCKER_STATE.UNBLOCK);
                        break;

                    default:
                        lastMessege = "Can not go to SCORE from " + currentState.toString();
                        return;
                }

                if(shouldTerminate(LIFT_STATES.SCORE)) return;
                currentState = targetState;

            } catch (Exception e){
                lastMessege = e.toString();
            }
        }
    };


}
