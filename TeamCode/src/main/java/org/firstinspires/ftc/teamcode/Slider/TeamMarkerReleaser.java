package org.firstinspires.ftc.teamcode.Slider;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class TeamMarkerReleaser {
    private Servo myServo;
    private LinearOpMode opMode;

    public TeamMarkerReleaser(LinearOpMode opMode){
        this.opMode = opMode;
        myServo = opMode.hardwareMap.get(Servo.class, "team_marker_releaser");
    }

    public void init(){
        myServo.setPosition(0);
    }

    public void release(){
        myServo.setPosition(1);
        try {
            Thread.sleep(350);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
