package org.firstinspires.ftc.teamcode.modes;

import com.qualcomm.hardware.rev.RevTouchSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Lift.Climber;
import org.firstinspires.ftc.teamcode.Lift.Lift;
import org.firstinspires.ftc.teamcode.Slider.Collector;
import org.firstinspires.ftc.teamcode.Slider.SliderJoint;
import org.firstinspires.ftc.teamcode.Tamara;


@TeleOp(name="Manual", group="Linear Opmode")
public class ManualMode extends LinearOpMode {

    Tamara tamara;

    double extendingSpeed = -300;


    @Override
    public void runOpMode() throws InterruptedException {

        tamara = new Tamara(this);

        waitForStart();

        tamara.manualInit();
        tamara.drivetrain.startManual();

        while(opModeIsActive()) {

            telemetry.clearAll();

            checkExtender();
            checkStateChange();


            telemetry.addLine(tamara.getStatus());

            telemetry.update();

            Thread.sleep(10);
        }

    }

    boolean firstA, firstY, firstRightStick, firstDpadUp, firstDpadDown,secondLeftStick, g2B;

    long collectorsStartTime = -1;

    void checkExtender(){
        if(tamara.currentState != Tamara.ROBOT_STATE.COLLECTING) return;

        try {

            if((int)(gamepad1.right_stick_y * extendingSpeed) != 0)
                tamara.slider.extender.moveWith((int)(gamepad1.right_stick_y * extendingSpeed));

            if(gamepad2.a) {
                if(firstA) {
                    firstA = false;
                    if(tamara.slider.sliderJoint.currentState == SliderJoint.SLIDER_JOINT_STATE.COLLECT) {
                        tamara.slider.collector.setState(Collector.COLLECTOR_STATE.STILL);
                        sleep(200);
                        tamara.slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.IDLE);
                    }
                    else {
                        tamara.slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.COLLECT);
                    }
                }
            } else  firstA = true;


            if(!gamepad2.b) {
                telemetry.addData("gamepad.b", gamepad2.b);
                telemetry.addLine(tamara.slider.collector.getStatus());
                g2B = false;
                if(tamara.slider.collector.currentState == Collector.COLLECTOR_STATE.OUTLET)
                    tamara.slider.collector.setState(Collector.COLLECTOR_STATE.INTAKE);
            }
            else if(!g2B){
                g2B = true;
                collectorsStartTime = System.currentTimeMillis();

                if(tamara.slider.collector.currentState == Collector.COLLECTOR_STATE.STILL)
                    tamara.slider.collector.setState(Collector.COLLECTOR_STATE.INTAKE);
                else
                    tamara.slider.collector.setState(Collector.COLLECTOR_STATE.STILL);
            }
            else if(collectorsStartTime != -1){

                if(System.currentTimeMillis() - collectorsStartTime >= 300) {
                    tamara.slider.collector.setState(Collector.COLLECTOR_STATE.OUTLET);
                    collectorsStartTime = -1;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void checkStateChange() throws InterruptedException {

        if(gamepad1.right_stick_button){
            if(firstRightStick){
                firstRightStick = false;
                if(tamara.currentState == Tamara.ROBOT_STATE.COLLECTING)
                    tamara.setState(Tamara.ROBOT_STATE.DEPOSIT);
            }

        } else firstRightStick = true;

        if(gamepad1.dpad_up){
            if(firstDpadUp){
                firstDpadUp = false;
                if(tamara.currentState == Tamara.ROBOT_STATE.COLLECTING)
                    tamara.setState(Tamara.ROBOT_STATE.PREPARE_TO_SCORE);
            }
        } else firstDpadUp = true;

        if(gamepad1.dpad_down){
            if(firstDpadDown){
                firstDpadDown = false;
                if(tamara.currentState != Tamara.ROBOT_STATE.COLLECTING)
                    tamara.setState(Tamara.ROBOT_STATE.COLLECTING);
            }
        } else firstDpadDown = true;

        if(gamepad2.y){
            if(secondLeftStick){
                secondLeftStick = false;
                if(tamara.currentState == Tamara.ROBOT_STATE.COLLECTING)
                    tamara.setState(Tamara.ROBOT_STATE.PREPARE_TO_SCORE);
            }
        } else secondLeftStick = true;

        if(gamepad1.y){
            if(firstY){
                firstY = false;
                if(tamara.currentState == Tamara.ROBOT_STATE.PREPARE_TO_LATCH)
                    tamara.setStateAndWait( Tamara.ROBOT_STATE.LATCH );
                else
                    tamara.setStateAndWait( Tamara.ROBOT_STATE.PREPARE_TO_LATCH );
            }

        } else firstY = true;
    }

}
