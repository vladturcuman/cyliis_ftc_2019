package org.firstinspires.ftc.teamcode.modes;


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.Slider.SliderJoint;

@Autonomous(name = "Crater", group = "Linear Opmode")
public class Crater extends AutonomusMode{
    @Override
    public void runOpMode() throws InterruptedException {
        super.runOpMode();
        if(!opModeIsActive()) return;

        tamara.drivetrain.moveWith(/*-45*/ 135, 30, 1);
        tamara.drivetrain.moveWith(/*35*/-145, 105, 1);
        tamara.drivetrain.moveWith(/*90*/ -90, 100, 1);

        tamara.teamMarkerReleaser.release();

        tamara.drivetrain.moveWith(/*-90*/ 90, 135, 1);

        try {
            tamara.slider.sliderJoint.setState(SliderJoint.SLIDER_JOINT_STATE.IDLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tamara.slider.extender.moveWith(1000);
    }
}
